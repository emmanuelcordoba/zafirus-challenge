<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Zafirus Challenge</title>
</head>
<body>
    <?php
        $inicios = [
            [[1,1],[16,16]],
            [[1,16],[16,1]],
            [[16,16],[1,1]],
            [[16,1],[1,16]],
        ];        
        $caballos = $inicios[random_int(0,count($inicios)-1)];    

        // Posiciones inidicales
        $juego = [];
        $juego[] = [
            'Pieza' => 'Caballo',
            'Color' => 'Blanco',
            'Posicion_X' => $caballos[0][0],
            'Posicion_Y' => $caballos[0][1],
        ];
        $juego[] = [
            'Pieza' => 'Caballo',
            'Color' => 'Negro',
            'Posicion_X' => $caballos[1][0],
            'Posicion_Y' => $caballos[1][1],
        ];
        $turno = 0;

        
        while ($caballos[0] !== $caballos[1]) { 
            $caballos[$turno] = siguientePosicion($caballos[$turno]);
            
            $juego[] = [
                'Pieza' => 'Caballo',
                'Color' => $turno ? 'Negro' : 'Blanco',
                'Posicion_X' => $caballos[$turno][0],
                'Posicion_Y' => $caballos[$turno][1],
            ];
            
            // Siguiente turno
            $turno++;
            if($turno > 1)
            {
                $turno = 0;
            }
        }
        
        echo 'GANADOR: '.($turno ? 'Negro' : 'Blanco');

        file_put_contents('juego.json', json_encode($juego));        

        function siguientePosicion($coord)
        {
            $movimientos = [[-2,-1],[-2,1],[2,-1], [2,1],[-1,-2],[-1,2],[1,-2], [1,2]];                       
            do{
                $movimiento = random_int(0,count($movimientos)-1);
                $next_coord = [$coord[0]+$movimientos[$movimiento][0],$coord[1]+$movimientos[$movimiento][1]];
            }while(!posicionValida($next_coord));            
            return $next_coord;
        }

        function posicionValida($coord)
        {
            return ($coord[0] >= 1 && $coord[0] <= 16) && ($coord[1] >= 1 && $coord[1] <= 16);
        }
    ?>
</body>
</html>